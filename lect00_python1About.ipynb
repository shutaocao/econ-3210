{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Python for Economics and Finance\n",
    "## 1. About Python\n",
    "### Shutao Cao\n",
    "#### 2024-01-08"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Outline\n",
    "- [Why Python](#Why-Python)\n",
    "- [The Python Ecosystem for Scientific Computing](#The-Python-Ecosystem-for-Scientific-Computing)\n",
    "- [Installing Python](#Installing-Python)\n",
    "- [What to use to program Python](#What-to-use-to-program-Python)\n",
    "- [Jupyter Notebooks](#Jupyter-Notebooks)\n",
    "- [Useful Resources](#Useful-Resources)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "## Why Python\n",
    "\n",
    "<a id='index-1'></a>\n",
    "\n",
    "[Python](https://www.python.org) is a generic scripting computer language used for many purposes\n",
    "- System administration\n",
    "- Web development\n",
    "- Game development  \n",
    "- Multimedia, natural language\n",
    "- Scientific computing\n",
    "- Data science and statistics\n",
    "- Machine Learning, ...\n",
    "\n",
    "Python is one of the most popular programming languages in the scientific community pretty much in all areas.\n",
    "\n",
    "Python is awarded the programming language of the year 2020.\n",
    "\n",
    "[Python trends](https://www.tiobe.com/tiobe-index/python/)\n",
    "\n",
    "Python is the top programming language for sciences and engineering since 2017, ranked by IEEE.\n",
    "\n",
    "[Top Programming Language in 2023](https://spectrum.ieee.org/the-top-programming-languages-2023)\n",
    "\n",
    "Economists have been rapidly learning to use Python in research and teaching, and it is no stranger to young PhD students and researchers.\n",
    "\n",
    "The ecosystem of Python developer and user community is huge, it is safe to say that Python is the computer language for (almost) everything."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Using Python in Economics\n",
    "\n",
    "Python can be used for computation and data analysis:\n",
    "- Computation, finding solution to a complicated economics model\n",
    "- Simulation, making prediction of an economics model\n",
    "- Data processing\n",
    "- Data Analysis:\n",
    "   - Estimation and inference\n",
    "   - Prediction using machine learning\n",
    "- Policy assessment, predicting\n",
    "\n",
    "Main advantages of Python over other programming languages:\n",
    "- **Batteries included**: Rich collection of numerical libraries and data tools.\n",
    "- **Easy to learn**: Economists are not trained as programmers, Python is handy and easy.\n",
    "- **Easy to read**: Python syntax is simple and intuitive.\n",
    "- **Universal**: Python is a language used for many different problems.\n",
    "  Learning Python avoids learning a new software for each new problem."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "jp-MarkdownHeadingCollapsed": true
   },
   "source": [
    "## The Python Ecosystem for Scientific Computing\n",
    "Python is one of the core languages of scientific computing. In fact, it is the modules (libraries) written in Python that have become the essential tools for scientific computing.\n",
    "\n",
    "Many of the Python modules are translated from numerical libraries written in `C` and `Fortran`.\n",
    "\n",
    "Its popularity in economics has been rising rapidly, a collaborated effort is [quantecon.org](https://quantecon.org).\n",
    "\n",
    "In Python, a module is a file that contains python definitions and statements. We need to import them when we need to use them.\n",
    "\n",
    "[Scikit-learn](https://scikit-learn.org/stable/index.html), a Python machine learning package, mostly for supervised learning. Some other machine learning libraries, such as TensorFlow for neural networks.\n",
    "\n",
    "- **Python**\n",
    "   - The Python language: flow control, data types (``string``, ``int``), data collections (lists, dictionaries), etc.\n",
    "   - The standard library: string processing, file management, simple network protocols.\n",
    "   - A large number of specialized modules or applications written in Python: web framework, etc.\n",
    "   - Development tools (automatic testing, documentation generation)\n",
    "- **Interactive environments**:\n",
    "   - **[IPython](https://ipython.org/)**, an advanced Python console.\n",
    "   - **[Jupyter](https://jupyter.org/)**, interactive programming in browser.\n",
    "- **Core computing libraries**\n",
    "     - **[NumPy](https://numpy.org/)**: Array operations, matrix analysis, linear algebra.\n",
    "     - **[SciPy](https://scipy.org/)** : Numerical routines for integration, function interpolation, optimization, regression, etc.\n",
    "     - **[Matplotlib](https://matplotlib.org/)** : 2-D visualization. Some other complements are [Bokeh](https://bokeh.org), [Plotly](https://plotly.com/python/), seaborn, etc.\n",
    "     - **Sympy** for symbolic mathematics.\n",
    "- **Data Science**:\n",
    "    - **[Pandas](https://pandas.pydata.org/)** for data processing.\n",
    "    - **[Statsmodels](https://www.statsmodels.org/stable/index.html)** for statistical estimation and inference.\n",
    "    - **Scikit-image** for image processing.\n",
    "    - **[Scikit-learn](https://scikit-learn.org/stable/index.html)** for machine learning."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Installing Python\n",
    "\n",
    "### If your operating system is a flavor of Linux,\n",
    "you already know how to install Python. To install a package as a user, type this in terminal\n",
    "```shell\n",
    "pip install spyder\n",
    "```\n",
    "will install `Spyder`, a development environment for Python.\n",
    "\n",
    "### If your operating system is macOS or Windows,\n",
    "\n",
    "I suggest to install [Miniconda](https://docs.conda.io/projects/miniconda/en/latest/miniconda-install.html), it is supposedly sufficient for this course. If any package is missing, it can be installed by typing:\n",
    "```shell\n",
    "conda install spyder\n",
    "```\n",
    "will install Spyder.\n",
    "\n",
    "The more complete package is [Anaconda](https://docs.conda.io/projects/conda/en/stable/user-guide/install/index.html), which includes thousands of packages.\n",
    "\n",
    "See this link for comparing the two: [https://docs.conda.io/projects/conda/en/stable/user-guide/install/download.html#anaconda-or-miniconda](https://docs.conda.io/projects/conda/en/stable/user-guide/install/download.html#anaconda-or-miniconda)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## What to use to program Python\n",
    "\n",
    "- Interactive\n",
    "  - [Ipython](https://ipython.org/)\n",
    "  - [Jupyter Qtconsole](https://qtconsole.readthedocs.io/en/stable/)\n",
    "- Development environments\n",
    "  - [Visual Studio Code](https://code.visualstudio.com/)\n",
    "  - [Spyder](https://www.spyder-ide.org/)\n",
    "- [Jupyter Lab](https://jupyter.org/) on local computer\n",
    "- Working Jupyter notebooks on [Google Colab](https://colab.research.google.com/)\n",
    "- Editors with less-than-full functions\n",
    "  - vim\n",
    "  - Emacs"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "source": [
    "## Jupyter Notebooks\n",
    "\n",
    "[IPython](https://ipython.org/) is an interactive shell for running Python codes, it also provides kernel for Jupyter\n",
    "\n",
    "[Jupyter](https://jupyter.org/) is browser-based interactive development environment. It has a simple variation Jupyter Notebook and a rich variation JupyterLab.\n",
    "\n",
    "Jupyter notebooks can be used to write books, papers, presentation slides, etc.\n",
    "\n",
    "To launch JupyterLab, in terminal, type:\n",
    "```shell\n",
    "jupyter lab\n",
    "```\n",
    "The browser then pops-up, the notebook runs at http://localhost:8888/\n",
    "\n",
    "The [Jupyter Lab](https://jupyter.org/) and [Jupyter Notebook](https://jupyter.org/) enable users to author notebook documents that include: \n",
    "- Live code\n",
    "- Interactive widgets\n",
    "- Plots\n",
    "- Narrative text\n",
    "- Equations\n",
    "- Images\n",
    "- Video\n",
    "\n",
    "Jupyter Lab and Jupyter Notebook combines three components:\n",
    "* **The notebook web application**: An interactive web application for writing and running code interactively and authoring notebook documents.\n",
    "* **Kernels**: Separate processes started by the notebook web application that runs users' code in a given language and returns output back to the notebook web application.\n",
    "* **Notebook documents**: Self-contained documents that contain a representation of all content visible in the notebook web application, including inputs and outputs of the computations, narrative text, equations, images, and rich media representations of objects. Each notebook document has its own kernel.\n",
    "\n",
    "Notebooks consist of a **sequence of cells**. Three cell types:\n",
    "\n",
    "* **Code cells:** Input and output of live code that is run in the kernel\n",
    "* **Markdown cells:** Narrative text with embedded LaTeX equations\n",
    "* **Raw cells:** Unformatted text that is included, without modification, when notebooks are converted to different formats using nbconvert\n",
    "\n",
    "Internally, notebook documents are **[JSON](https://en.wikipedia.org/wiki/JSON) data** with **binary values [base64](https://en.wikipedia.org/wiki/Base64)** encoded. This allows them to be **read and manipulated programmatically** by any programming language. Because JSON is a text format, notebook documents are version control friendly.\n",
    "\n",
    "**Notebooks can be exported** to different static formats including HTML, reStructeredText, LaTeX, PDF, and slide shows ([reveal.js](https://revealjs.com)) using Jupyter's `nbconvert` utility.\n",
    "\n",
    "Furthermore, any notebook document available from a **public URL or on GitHub can be shared** via [nbviewer](https://nbviewer.jupyter.org). This service loads the notebook document from the URL and renders it as a static web page. The resulting web page may be shared with others **without their needing to install the Jupyter Notebook**.\n",
    "\n",
    "Through Jupyter's kernel and messaging architecture, the Jupyter Lab and Notebook allows code to be run in a range of different programming languages. For each notebook document that a user opens, the web application starts a kernel that runs the code for that notebook. Each kernel is capable of running code in a single programming language and there are kernels available in the following languages:\n",
    "\n",
    "* Python(https://github.com/ipython/ipython)\n",
    "* Julia (https://github.com/JuliaLang/IJulia.jl)\n",
    "* R (https://github.com/IRkernel/IRkernel)\n",
    "\n",
    "The default kernel runs Python code. The notebook provides a simple way for users to pick which of these kernels is used for a given notebook. \n",
    "\n",
    "Each of these kernels communicate with the notebook web application and web browser using a JSON over ZeroMQ/WebSockets message protocol that is described [here](https://jupyter-client.readthedocs.io/en/latest/messaging.html#messaging). Most users don't need to know about these details."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Code cell"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Hello, world!\n"
     ]
    }
   ],
   "source": [
    "# This is a code cell, and a line starting with # is a comment line\n",
    "print(\"Hello, world!\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "a = 2 and b = 3\n",
      "β = 1\n"
     ]
    }
   ],
   "source": [
    "a = 2\n",
    "b = a+1\n",
    "print(\"a =\",a, \"and b =\",b)\n",
    "\n",
    "# Python supports unicode characters\n",
    "# To display Greek letter beta: type \\beta then hit the Tab key\n",
    "β = 3*b - 8\n",
    "print(\"β =\",β)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Access documentation with ?\n",
    "\n",
    "In a code cell, a question mark ? following a function or module name will show documentation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Length of x is 5\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "\u001b[0;31mSignature:\u001b[0m \u001b[0mlen\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mobj\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0;34m/\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
       "\u001b[0;31mDocstring:\u001b[0m Return the number of items in a container.\n",
       "\u001b[0;31mType:\u001b[0m      builtin_function_or_method"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "x = [1,2,3,4,5]\n",
    "print(\"Length of x is\", len(x))\n",
    "# What is len?\n",
    "len?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "\u001b[0;31mSignature:\u001b[0m \u001b[0mx\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mappend\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mobject\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0;34m/\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
       "\u001b[0;31mDocstring:\u001b[0m Append object to the end of the list.\n",
       "\u001b[0;31mType:\u001b[0m      builtin_function_or_method"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "# One more example\n",
    "x.append?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### TAB completion"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# from scipy import <TAB>\n",
    "# will show list of modules in scipy"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "#x.<TAB>\n",
    "# will show a list of functions "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## See what are included in a library"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "['__doc__', '__file__', '__loader__', '__name__', '__package__', '__spec__', 'acos', 'acosh', 'asin', 'asinh', 'atan', 'atan2', 'atanh', 'cbrt', 'ceil', 'comb', 'copysign', 'cos', 'cosh', 'degrees', 'dist', 'e', 'erf', 'erfc', 'exp', 'exp2', 'expm1', 'fabs', 'factorial', 'floor', 'fmod', 'frexp', 'fsum', 'gamma', 'gcd', 'hypot', 'inf', 'isclose', 'isfinite', 'isinf', 'isnan', 'isqrt', 'lcm', 'ldexp', 'lgamma', 'log', 'log10', 'log1p', 'log2', 'modf', 'nan', 'nextafter', 'perm', 'pi', 'pow', 'prod', 'radians', 'remainder', 'sin', 'sinh', 'sqrt', 'sumprod', 'tan', 'tanh', 'tau', 'trunc', 'ulp']\n"
     ]
    }
   ],
   "source": [
    "import math\n",
    "# Uncomment the line below to see output\n",
    "print(dir(math))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "# Uncomment the line below to see output\n",
    "# print(np.__all__)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Markdown cell\n",
    "The three hashes means the line above is a heading of level 3. Level 1 heading uses one hash.\n",
    "\n",
    "[Markdown](https://jupyter-notebook.readthedocs.io/en/stable/examples/Notebook/Working%20With%20Markdown%20Cells.html#) is a simple mark-up language, we can use it to write formatted text and equations.\n",
    "\n",
    "To embed Python code in a markdown cell, quote the code block with \"```\"\n",
    "```python\n",
    "import numpy as np\n",
    "x = np.array(\n",
    "```\n",
    "\n",
    "To write equations etc., use LaTeX syntax.\n",
    "\n",
    "The **Fundamental Theorem of Calculus** _states_ that\n",
    "$$\\int_a^b f(x) = F(a) - F(b).$$\n",
    "\n",
    "Embed an image from a local folder or a website:\n",
    "\n",
    "![Python logo](https://www.python.org/static/img/python-logo.png)\n",
    "\n",
    "We can also embed animation files\n",
    "\n",
    "<video controls src=\"animation_ParetoOptimum.mp4\">animation</video>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Useful Resources\n",
    "\n",
    "- [Python Tutorials](https://docs.python.org/3/tutorial/index.html)\n",
    "- [QuantEcon](https://quantecon.org/lectures/)"
   ]
  }
 ],
 "metadata": {
  "date": 1609905266.8798778,
  "filename": "about_py.md",
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.1"
  },
  "title": "About Python"
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
